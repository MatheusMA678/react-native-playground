import { StatusBar, ImageBackground } from 'react-native';
import { StartScreen } from './pages/StartScreen'
import { Container } from './styles/globalStyles'

import BgImage from './images/bg2.png'

export default function App() {
  return (
    <Container>
      <ImageBackground source={BgImage} style={{flex: 1, resizeMode: 'cover', justifyContent: 'center', padding: 55}}>
        <StartScreen />
        <StatusBar barStyle={'default'} backgroundColor={'#FF4B3A'} />
      </ImageBackground>
    </Container>
  );
}

// '#FF4B3A'