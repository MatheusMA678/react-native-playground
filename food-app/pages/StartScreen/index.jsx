import { View, Text, Image } from 'react-native'
import { useFonts } from 'expo-font';
import React from 'react'
import { Circle, Container, Button, Title, ButtonTitle } from './styles'

import AppIcon from '../../images/icon.png'

export function StartScreen() {
  const [fontsLoaded] = useFonts({
    'SF-Pro-Rounded-Heavy': require('../../fonts/sf-pro-rounded/SF-Pro-Rounded-Heavy.otf'),
    'SF-Pro-Text-Semibold': require('../../fonts/sf-pro-text/SF-Pro-Text-Semibold.otf'),
  });

  if (!fontsLoaded) {
    return <Text>Loading fonts...</Text>
  }

  return (
    <Container>
      <View>
        <Circle>
          <Image source={AppIcon} alt={'app icon'} />
        </Circle>
        <Title>Food for Everyone</Title>
      </View>
      <Button onPress={() => null} >
        <ButtonTitle>Get Started</ButtonTitle>
      </Button>
    </Container>
  )
}