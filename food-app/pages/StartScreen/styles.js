import styled from 'styled-components/native'

export const Container = styled.View`
  flex: 1;
  // justify-content: center;
  justify-content: space-between;
`

export const Circle = styled.View`
  background-color: white;
  border-radius: 35px;
  width: 70px;
  height: 70px;
  margin-bottom: 30px;
  justify-content: center;
  align-items: center;
`

export const Title = styled.Text`
  color: white;
  font-size: 65px;
  font-family: 'SF-Pro-Rounded-Heavy';
`

export const Button = styled.Pressable`
  background-color: ${({pressed}) => pressed ? '#eee' : 'white'};
  height: 70px;
  border-radius: 35px;
  justify-content: center;
  align-items: center;
  transition: 0.2s;
  
`

export const ButtonTitle = styled.Text`
  color: #FF460A;
  font-family: 'SF-Pro-Text-Semibold';
  font-size: 16px;
`
